---
author: George McNinch
title: Bio
---

+ Find a [more personal page, here](https://stencilv.gitlab.io/web/)

+ I study and teach
  [mathematics](http://gmcninch-tufts.github.io/math); I'm a professor
  in the [math department](http://math.tufts.edu) at [Tufts
  University](http://www.tufts.edu).

+ And I have a hobbyist-interest in [`GNU/Linux`] and [`Emacs`] and
  coding -- including [`Haskell`], [`Purescript`], [`OCaml`]. Thus, this
  blog.

[Stencilv]: /posts/2019-05--stencilv.html
[`GNU/Linux`]: http://www.gnu.org
[`Ocaml`]: http://ocaml.org
[`Haskell`]: https://www.haskell.org/
[`Purescript`]: https://www.purescript.org/
[`Emacs`]: https://www.gnu.org/software/emacs/


