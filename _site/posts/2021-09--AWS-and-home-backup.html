<!doctype html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> AWS simple storage for home computing backup </title>
        <link rel="stylesheet" href="../css/default.css" />
        <link rel="stylesheet" href="../css/syntax.css" />
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js">
		<!-- src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML"> -->
	</script>	
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700" type="text/css" rel="stylesheet" media="screen" />
	<link href="https://fonts.googleapis.com/css?family=Arimo:400,700" type="text/css" rel="stylesheet" media="screen" />
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" type="text/css" rel="stylesheet" media="screen" />
	<link href="https://fonts.googleapis.com/css?family=Inconsolata:400,700" type="text/css" rel="stylesheet" media="screen" />
	<link href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" type="text/css" rel="stylesheet" media="screen" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous" />	
    </head>
    <body>
        <header>
            <div class="logo">
              <a href="../">Decoded? <i class="fas fa-desktop"></i>
	      </a>
            </div>
            <nav>
              <a href="../pages/Bio.html">About</a>
              <a href="../archive.html">Archive</a>
            </nav>
        </header>

        <main role="main">
            <h1>AWS simple storage for home computing backup</h1>
            <article>
    <section class="header">
        Posted on 2021-09-08
        
    </section>
    <div class="header">
    
       Tags: <a title="All pages tagged 'home-computing'." href="../tags/home-computing.html">home-computing</a>
    
    </div>    
    <section>
        <p>I have a confession to make: for years, I’ve been using <em>redundancy</em>
for safe-keeping my (and my family’s) home computing data – I use
<a href="https://www.cis.upenn.edu/~bcpierce/unison/"><code>unison</code></a> via <code>cron</code>
jobs to keep data synced between several home machines (plus external
hard-drives). But of course, optimally one wants <em>off-site</em> backups.</p>
<p>As others have observed:</p>
<blockquote>
<p><a href="https://www.cloudwards.net/best-online-backup-for-linux/">A lot of the top backup providers are incompatible with Linux,
which severely limits your
options.</a></p>
</blockquote>
<p>A couple months back, I tried <a href="https://www.idrive.com/"><code>IDrive</code></a>
which seemed reasonable, but was slightly frustrating (for me) in a
few ways. Mostly, I didn’t find it easy to automate everything that I
wanted to automate.</p>
<p>Subsequently, I’ve landed on using <a href="https://aws.amazon.com/s3/"><code>AWS</code> simple
storage</a>.</p>
<p>Now, I’m more-or-less a month and a half in using <code>AWS</code>. I’m happy
with it, and confident that it is “doing what I want it to do”.</p>
<p>I’m sure I’m going to spend a bit more each year than I would have
with <code>IDrive</code>, but not a <em>ton</em> more.</p>
<h1 id="advice">Advice</h1>
<p>Here are some comments that might be useful for someone considering
use of <code>AWS</code> for a “home computing” use-case:</p>
<ul>
<li><p>Use of <code>AWS</code> for <em>backup</em> was for me finally the motivation to move
current projects to <code>github/gitlab</code>. This may sound a bit odd, but
to date <code>rsync/unison</code> were mostly “good enough” for me.</p>
<p>Unless one implements something like <code>.gitignore</code>, coding projects
can create a lot of churn in backups.</p></li>
<li><p>I’m using the <a href="https://aws.amazon.com/s3/storage-classes/?nc=sn&amp;loc=3"><code>Infrequent access tier</code>
storage</a>. The
costs per gigabyte are lower than <code>standard tier</code>, though there <em>per
access</em> charges. One consequence is that my costs were higher in the
first month (there were costs for instantiating the backups).</p></li>
<li><p>One silly <em>mistake</em> I made at first was that there were several
symlinks (e.g. to large photo directories) in some of my directory
structures. These caused some (large) redundant backups, which I
didn’t notice for a while.</p></li>
</ul>
<h1 id="back-up-procedures">Back-up procedures</h1>
<p>In <a href="https://www.debian.org/"><code>Debian GNU/Linux</code></a>, one can install the
command line tools for interacting with <code>AWS</code> via:</p>
<pre><code>$ sudo apt install awscli</code></pre>
<p>Use of <code>awscli</code> requires one to store some <em>credentials</em> in <code>~/.aws</code>.
I have several user accounts to backup, so each user needs an <code>~/.aws</code>
directory. Now, each user has a <code>cron</code> entry (for me, currently
scheduled <span class="math inline">\(2\times\)</span>/week) which runs <code>aws sync</code> on relevant
directories.</p>
<p>I’ve included below the python script I use to invoke <code>aws sync</code>;
this script is configured by a <code>json</code> file formatted as follows:</p>
<p><code>aws.json</code></p>
<div class="sourceCode" id="cb2"><pre class="sourceCode json"><code class="sourceCode json"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="fu">{</span> <span class="dt">&quot;settings&quot;</span><span class="fu">:</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a>  <span class="fu">{</span> <span class="dt">&quot;exclude_paths&quot;</span><span class="fu">:</span> <span class="ot">[</span> <span class="st">&quot;emacs/straight/*&quot;</span><span class="ot">,</span></span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a>                       <span class="st">&quot;backups/emacs-backups-*&quot;</span><span class="ot">,</span></span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a>                       <span class="st">&quot;elfeed/*&quot;</span><span class="ot">,</span></span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a>                       <span class="st">&quot;user-emacs-directory/*&quot;</span></span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true" tabindex="-1"></a>                     <span class="ot">]</span><span class="fu">,</span></span>
<span id="cb2-7"><a href="#cb2-7" aria-hidden="true" tabindex="-1"></a>    <span class="dt">&quot;log_location&quot;</span><span class="fu">:</span> <span class="st">&quot;assets/logs/aws-logs.log&quot;</span></span>
<span id="cb2-8"><a href="#cb2-8" aria-hidden="true" tabindex="-1"></a>  <span class="fu">},</span></span>
<span id="cb2-9"><a href="#cb2-9" aria-hidden="true" tabindex="-1"></a>  <span class="dt">&quot;users&quot;</span><span class="fu">:</span></span>
<span id="cb2-10"><a href="#cb2-10" aria-hidden="true" tabindex="-1"></a>  <span class="fu">{</span> <span class="dt">&quot;alice&quot;</span><span class="fu">:</span> </span>
<span id="cb2-11"><a href="#cb2-11" aria-hidden="true" tabindex="-1"></a>    <span class="fu">{</span> <span class="dt">&quot;bucket&quot;</span> <span class="fu">:</span> <span class="st">&quot;bucket-alice&quot;</span><span class="fu">,</span></span>
<span id="cb2-12"><a href="#cb2-12" aria-hidden="true" tabindex="-1"></a>      <span class="dt">&quot;dirs&quot;</span> <span class="fu">:</span> <span class="ot">[</span> <span class="st">&quot;assets&quot;</span><span class="ot">,</span></span>
<span id="cb2-13"><a href="#cb2-13" aria-hidden="true" tabindex="-1"></a>                 <span class="st">&quot;Documents&quot;</span><span class="ot">,</span></span>
<span id="cb2-14"><a href="#cb2-14" aria-hidden="true" tabindex="-1"></a>                 <span class="st">&quot;Code&quot;</span><span class="ot">,</span></span>
<span id="cb2-15"><a href="#cb2-15" aria-hidden="true" tabindex="-1"></a>                 <span class="st">&quot;Practical&quot;</span></span>
<span id="cb2-16"><a href="#cb2-16" aria-hidden="true" tabindex="-1"></a>               <span class="ot">]</span></span>
<span id="cb2-17"><a href="#cb2-17" aria-hidden="true" tabindex="-1"></a>    <span class="fu">},</span></span>
<span id="cb2-18"><a href="#cb2-18" aria-hidden="true" tabindex="-1"></a>    <span class="dt">&quot;bob&quot;</span><span class="fu">:</span> </span>
<span id="cb2-19"><a href="#cb2-19" aria-hidden="true" tabindex="-1"></a>    <span class="fu">{</span> <span class="dt">&quot;bucket&quot;</span> <span class="fu">:</span> <span class="st">&quot;bucket-bob&quot;</span><span class="fu">,</span></span>
<span id="cb2-20"><a href="#cb2-20" aria-hidden="true" tabindex="-1"></a>      <span class="dt">&quot;dirs&quot;</span> <span class="fu">:</span> <span class="ot">[</span> <span class="st">&quot;assets&quot;</span><span class="ot">,</span></span>
<span id="cb2-21"><a href="#cb2-21" aria-hidden="true" tabindex="-1"></a>                 <span class="st">&quot;Code&quot;</span><span class="ot">,</span></span>
<span id="cb2-22"><a href="#cb2-22" aria-hidden="true" tabindex="-1"></a>                 <span class="st">&quot;Library&quot;</span><span class="ot">,</span></span>
<span id="cb2-23"><a href="#cb2-23" aria-hidden="true" tabindex="-1"></a>                 <span class="st">&quot;family-photos&quot;</span></span>
<span id="cb2-24"><a href="#cb2-24" aria-hidden="true" tabindex="-1"></a>               <span class="ot">]</span></span>
<span id="cb2-25"><a href="#cb2-25" aria-hidden="true" tabindex="-1"></a>    <span class="fu">}</span></span>
<span id="cb2-26"><a href="#cb2-26" aria-hidden="true" tabindex="-1"></a><span class="fu">}</span></span></code></pre></div>
<p>Here is the script (which also performs some <em>logging</em>).</p>
<p><code>aws-sync.py</code></p>
<div class="sourceCode" id="cb3"><pre class="sourceCode python"><code class="sourceCode python"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="co">#!/usr/bin/env python3</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a><span class="co"># Time-stamp: &lt;2021-09-07 Tue 08:48 EDT - george@valhalla&gt;</span></span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true" tabindex="-1"></a><span class="im">import</span> os</span>
<span id="cb3-5"><a href="#cb3-5" aria-hidden="true" tabindex="-1"></a><span class="im">import</span> argparse</span>
<span id="cb3-6"><a href="#cb3-6" aria-hidden="true" tabindex="-1"></a><span class="im">import</span> logging</span>
<span id="cb3-7"><a href="#cb3-7" aria-hidden="true" tabindex="-1"></a><span class="im">import</span> json</span>
<span id="cb3-8"><a href="#cb3-8" aria-hidden="true" tabindex="-1"></a><span class="im">from</span> subprocess <span class="im">import</span> run</span>
<span id="cb3-9"><a href="#cb3-9" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-10"><a href="#cb3-10" aria-hidden="true" tabindex="-1"></a>parser <span class="op">=</span> argparse.ArgumentParser(description<span class="op">=</span><span class="st">'AWS syncing.'</span>)</span>
<span id="cb3-11"><a href="#cb3-11" aria-hidden="true" tabindex="-1"></a>parser.add_argument(<span class="st">'-d'</span>,<span class="st">'--dryrun'</span>,</span>
<span id="cb3-12"><a href="#cb3-12" aria-hidden="true" tabindex="-1"></a>                    action<span class="op">=</span><span class="st">'store_const'</span>,</span>
<span id="cb3-13"><a href="#cb3-13" aria-hidden="true" tabindex="-1"></a>                    const<span class="op">=</span><span class="st">'--dryrun'</span>,</span>
<span id="cb3-14"><a href="#cb3-14" aria-hidden="true" tabindex="-1"></a>                    default<span class="op">=</span><span class="st">''</span>,</span>
<span id="cb3-15"><a href="#cb3-15" aria-hidden="true" tabindex="-1"></a>                    <span class="bu">help</span><span class="op">=</span><span class="st">'dryrun'</span>)</span>
<span id="cb3-16"><a href="#cb3-16" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-17"><a href="#cb3-17" aria-hidden="true" tabindex="-1"></a><span class="kw">def</span> makeExcludeString(exclude_paths):</span>
<span id="cb3-18"><a href="#cb3-18" aria-hidden="true" tabindex="-1"></a>    <span class="cf">return</span> <span class="st">&quot; &quot;</span>.join(<span class="bu">map</span>(<span class="kw">lambda</span> s: <span class="ss">f&quot;--exclude </span><span class="ch">\&quot;</span><span class="sc">{</span>s<span class="sc">}</span><span class="ch">\&quot;</span><span class="ss">&quot;</span>,exclude_paths))</span>
<span id="cb3-19"><a href="#cb3-19" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-20"><a href="#cb3-20" aria-hidden="true" tabindex="-1"></a><span class="kw">def</span> getconfig(filepath):</span>
<span id="cb3-21"><a href="#cb3-21" aria-hidden="true" tabindex="-1"></a>    f <span class="op">=</span> <span class="bu">open</span>(filepath)</span>
<span id="cb3-22"><a href="#cb3-22" aria-hidden="true" tabindex="-1"></a>    contents <span class="op">=</span> f.read()</span>
<span id="cb3-23"><a href="#cb3-23" aria-hidden="true" tabindex="-1"></a>    <span class="cf">return</span> json.loads(contents)</span>
<span id="cb3-24"><a href="#cb3-24" aria-hidden="true" tabindex="-1"></a>    </span>
<span id="cb3-25"><a href="#cb3-25" aria-hidden="true" tabindex="-1"></a><span class="kw">def</span> sync(bucket,dirname,exclude_paths,dryrun):</span>
<span id="cb3-26"><a href="#cb3-26" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-27"><a href="#cb3-27" aria-hidden="true" tabindex="-1"></a>    excludes  <span class="op">=</span> makeExcludeString( exclude_paths )</span>
<span id="cb3-28"><a href="#cb3-28" aria-hidden="true" tabindex="-1"></a>    </span>
<span id="cb3-29"><a href="#cb3-29" aria-hidden="true" tabindex="-1"></a>    aws <span class="op">=</span> <span class="st">&quot; &quot;</span>.join([<span class="ss">f&quot;/usr/bin/aws s3 sync&quot;</span>,</span>
<span id="cb3-30"><a href="#cb3-30" aria-hidden="true" tabindex="-1"></a>                    <span class="ss">f&quot;$HOME/</span><span class="sc">{</span>dirname<span class="sc">}</span><span class="ss"> s3://</span><span class="sc">{</span>bucket<span class="sc">}</span><span class="ss">/</span><span class="sc">{</span>dirname<span class="sc">}</span><span class="ss"> </span><span class="sc">{</span>dryrun<span class="sc">}</span><span class="ss">&quot;</span>,</span>
<span id="cb3-31"><a href="#cb3-31" aria-hidden="true" tabindex="-1"></a>                    <span class="ss">f&quot;--delete --storage-class STANDARD_IA </span><span class="sc">{</span>excludes<span class="sc">}</span><span class="ss">&quot;</span>,</span>
<span id="cb3-32"><a href="#cb3-32" aria-hidden="true" tabindex="-1"></a>                    <span class="ss">f&quot;--no-progress&quot;</span></span>
<span id="cb3-33"><a href="#cb3-33" aria-hidden="true" tabindex="-1"></a>                    ])</span>
<span id="cb3-34"><a href="#cb3-34" aria-hidden="true" tabindex="-1"></a>    cp<span class="op">=</span> run(aws,shell<span class="op">=</span><span class="va">True</span>,capture_output<span class="op">=</span><span class="va">True</span>,text<span class="op">=</span><span class="va">True</span>)</span>
<span id="cb3-35"><a href="#cb3-35" aria-hidden="true" tabindex="-1"></a>    <span class="cf">return</span> cp.stdout</span>
<span id="cb3-36"><a href="#cb3-36" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-37"><a href="#cb3-37" aria-hidden="true" tabindex="-1"></a><span class="kw">def</span> indent(text,n):</span>
<span id="cb3-38"><a href="#cb3-38" aria-hidden="true" tabindex="-1"></a>    <span class="cf">return</span> <span class="st">&quot;</span><span class="ch">\n</span><span class="st">&quot;</span>.join(<span class="bu">map</span>(<span class="kw">lambda</span> s:(n<span class="op">*</span><span class="st">' '</span>)<span class="op">+</span>s,<span class="bu">str</span>.split(text,<span class="st">&quot;</span><span class="ch">\n</span><span class="st">&quot;</span>)))</span>
<span id="cb3-39"><a href="#cb3-39" aria-hidden="true" tabindex="-1"></a>    </span>
<span id="cb3-40"><a href="#cb3-40" aria-hidden="true" tabindex="-1"></a><span class="cf">if</span> <span class="va">__name__</span> <span class="op">==</span> <span class="st">&quot;__main__&quot;</span>:</span>
<span id="cb3-41"><a href="#cb3-41" aria-hidden="true" tabindex="-1"></a>    args <span class="op">=</span> parser.parse_args()</span>
<span id="cb3-42"><a href="#cb3-42" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-43"><a href="#cb3-43" aria-hidden="true" tabindex="-1"></a>    conf <span class="op">=</span> getconfig(<span class="st">&quot;/home/george/assets/config/aws.json&quot;</span>)</span>
<span id="cb3-44"><a href="#cb3-44" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-45"><a href="#cb3-45" aria-hidden="true" tabindex="-1"></a>    logfile <span class="op">=</span> os.path.join(os.environ[<span class="st">'HOME'</span>],conf[<span class="st">&quot;settings&quot;</span>][<span class="st">&quot;log_location&quot;</span>])</span>
<span id="cb3-46"><a href="#cb3-46" aria-hidden="true" tabindex="-1"></a>    </span>
<span id="cb3-47"><a href="#cb3-47" aria-hidden="true" tabindex="-1"></a>    logging.basicConfig(filename<span class="op">=</span>logfile,</span>
<span id="cb3-48"><a href="#cb3-48" aria-hidden="true" tabindex="-1"></a>                        encoding<span class="op">=</span><span class="st">'utf-8'</span>,</span>
<span id="cb3-49"><a href="#cb3-49" aria-hidden="true" tabindex="-1"></a>                        level<span class="op">=</span>logging.DEBUG,</span>
<span id="cb3-50"><a href="#cb3-50" aria-hidden="true" tabindex="-1"></a>                        <span class="bu">format</span><span class="op">=</span><span class="st">'</span><span class="sc">%(asctime)s</span><span class="st"> </span><span class="sc">%(message)s</span><span class="st">'</span>,</span>
<span id="cb3-51"><a href="#cb3-51" aria-hidden="true" tabindex="-1"></a>                        datefmt<span class="op">=</span><span class="st">'%Y-%m-</span><span class="sc">%d</span><span class="st"> %H:%M:%S'</span>,</span>
<span id="cb3-52"><a href="#cb3-52" aria-hidden="true" tabindex="-1"></a>                        )</span>
<span id="cb3-53"><a href="#cb3-53" aria-hidden="true" tabindex="-1"></a>    logging.info(<span class="st">&quot;syncing aws&quot;</span>)</span>
<span id="cb3-54"><a href="#cb3-54" aria-hidden="true" tabindex="-1"></a>    </span>
<span id="cb3-55"><a href="#cb3-55" aria-hidden="true" tabindex="-1"></a>    bucket <span class="op">=</span> conf[<span class="st">'users'</span>][os.environ[<span class="st">'USER'</span>]][<span class="st">&quot;bucket&quot;</span>]</span>
<span id="cb3-56"><a href="#cb3-56" aria-hidden="true" tabindex="-1"></a>  </span>
<span id="cb3-57"><a href="#cb3-57" aria-hidden="true" tabindex="-1"></a>    <span class="cf">for</span> d <span class="kw">in</span> conf[<span class="st">'users'</span>][os.environ[<span class="st">'USER'</span>]][<span class="st">&quot;dirs&quot;</span>]:</span>
<span id="cb3-58"><a href="#cb3-58" aria-hidden="true" tabindex="-1"></a>        res<span class="op">=</span>sync(bucket,d,conf[<span class="st">&quot;settings&quot;</span>][<span class="st">&quot;exclude_paths&quot;</span>],args.dryrun)</span>
<span id="cb3-59"><a href="#cb3-59" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-60"><a href="#cb3-60" aria-hidden="true" tabindex="-1"></a>        logging.info(<span class="ss">f&quot;Syncing directory </span><span class="sc">{</span>d<span class="sc">}</span><span class="ss"> with AWS bucket </span><span class="sc">{</span>bucket<span class="sc">}</span><span class="ss">.&quot;</span>)</span>
<span id="cb3-61"><a href="#cb3-61" aria-hidden="true" tabindex="-1"></a>        logging.info(<span class="ss">f&quot;output:</span><span class="ch">\n</span><span class="sc">{</span>indent(res,<span class="dv">3</span>)<span class="sc">}</span><span class="ss">&quot;</span>)</span></code></pre></div>
    </section>
</article>

        </main>

    </body>
    <footer>
      <nav>
	<a href="https://stencilv.gitlab.io/web/" title="Personal page">
	  <i class="fa fa-bicycle"> </i>
        </a>	
	<a href="https://gmcninch-tufts.github.io/math/" title="Professional page">
	  <i class="fas fa-chalkboard"> </i>
        </a>
	<a href="https://mathstodon.xyz/@stencilv" rel="me" title="mastodon">
	  <i class="fab fa-mastodon"></i>
	</a>	
	<a href="https://gitlab.com/gmcninch" title="gitlab">
	  <i class="fab fa-gitlab"></i>	  
	</a>
	<a href="https://github.com/gmcninch" title="github">
	  <i class="fab fa-github"></i>	  
	</a>	
      </nav>
    </footer>
</html>
