---
title: AWS simple storage for home computing backup
date: 2021-09-08
tags: home-computing
---

I have a confession to make: for years, I've been using *redundancy*
for safe-keeping my (and my family's) home computing data -- I use
[`unison`](https://www.cis.upenn.edu/~bcpierce/unison/) via `cron`
jobs to keep data synced between several home machines (plus external
hard-drives). But of course, optimally one wants *off-site* backups.

As others have observed:

> [A lot of the top backup providers are incompatible with Linux,
> which severely limits your
> options.](https://www.cloudwards.net/best-online-backup-for-linux/)

A couple months back, I tried [`IDrive`](https://www.idrive.com/)
which seemed reasonable, but was slightly frustrating (for me) in a
few ways. Mostly, I didn't find it easy to automate everything that I
wanted to automate.

Subsequently, I've landed on using [`AWS` simple
storage](https://aws.amazon.com/s3/).

Now, I'm more-or-less a month and a half in using `AWS`. I'm happy
with it, and confident that it is "doing what I want it to do".

I'm sure I'm going to spend a bit more each year than I would have
with `IDrive`, but not a *ton* more.

# Advice

Here are some comments that might be useful for someone considering
use of `AWS` for a "home computing" use-case:

- Use of `AWS` for *backup* was for me finally the motivation to move
  current projects to `github/gitlab`. This may sound a bit odd, but
  to date `rsync/unison` were mostly "good enough" for me. 
  
  Unless one implements something like `.gitignore`, coding projects
  can create a lot of churn in backups.

- I'm using the [`Infrequent access tier`
  storage](https://aws.amazon.com/s3/storage-classes/?nc=sn&loc=3). The
  costs per gigabyte are lower than `standard tier`, though there *per
  access* charges. One consequence is that my costs were higher in the
  first month (there were costs for instantiating the backups).

- One silly *mistake* I made at first was that there were several
  symlinks (e.g. to large photo directories) in some of my directory
  structures. These caused some (large) redundant backups, which I
  didn't notice for a while.

# Back-up procedures

In [`Debian GNU/Linux`](https://www.debian.org/), one can install the
command line tools for interacting with `AWS` via:

```
$ sudo apt install awscli
```

Use of `awscli` requires one to store some *credentials* in `~/.aws`.
I have several user accounts to backup, so each user needs an `~/.aws`
directory.  Now, each user has a `cron` entry (for me, currently
scheduled $2\times$/week) which runs `aws sync` on relevant
directories.

I've included below the python script I use to invoke `aws sync`;
this script  is configured by a `json` file formatted as follows:


`aws.json`
```json
{ "settings":
  { "exclude_paths": [ "emacs/straight/*",
                       "backups/emacs-backups-*",
                       "elfeed/*",
                       "user-emacs-directory/*"
                     ],
    "log_location": "assets/logs/aws-logs.log"
  },
  "users":
  { "alice": 
    { "bucket" : "bucket-alice",
      "dirs" : [ "assets",
                 "Documents",
                 "Code",
                 "Practical"
               ]
    },
    "bob": 
    { "bucket" : "bucket-bob",
      "dirs" : [ "assets",
                 "Code",
                 "Library",
                 "family-photos"
               ]
    }
}
```

Here is the script (which also performs some *logging*).

`aws-sync.py`
```python
#!/usr/bin/env python3
# Time-stamp: <2021-09-07 Tue 08:48 EDT - george@valhalla>

import os
import argparse
import logging
import json
from subprocess import run

parser = argparse.ArgumentParser(description='AWS syncing.')
parser.add_argument('-d','--dryrun',
                    action='store_const',
                    const='--dryrun',
                    default='',
                    help='dryrun')

def makeExcludeString(exclude_paths):
    return " ".join(map(lambda s: f"--exclude \"{s}\"",exclude_paths))

def getconfig(filepath):
    f = open(filepath)
    contents = f.read()
    return json.loads(contents)
    
def sync(bucket,dirname,exclude_paths,dryrun):

    excludes  = makeExcludeString( exclude_paths )
    
    aws = " ".join([f"/usr/bin/aws s3 sync",
                    f"$HOME/{dirname} s3://{bucket}/{dirname} {dryrun}",
                    f"--delete --storage-class STANDARD_IA {excludes}",
                    f"--no-progress"
                    ])
    cp= run(aws,shell=True,capture_output=True,text=True)
    return cp.stdout

def indent(text,n):
    return "\n".join(map(lambda s:(n*' ')+s,str.split(text,"\n")))
    
if __name__ == "__main__":
    args = parser.parse_args()

    conf = getconfig("/home/george/assets/config/aws.json")

    logfile = os.path.join(os.environ['HOME'],conf["settings"]["log_location"])
    
    logging.basicConfig(filename=logfile,
                        encoding='utf-8',
                        level=logging.DEBUG,
                        format='%(asctime)s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        )
    logging.info("syncing aws")
    
    bucket = conf['users'][os.environ['USER']]["bucket"]
  
    for d in conf['users'][os.environ['USER']]["dirs"]:
        res=sync(bucket,d,conf["settings"]["exclude_paths"],args.dryrun)

        logging.info(f"Syncing directory {d} with AWS bucket {bucket}.")
        logging.info(f"output:\n{indent(res,3)}")

```

