---
title   : Advent of Code 2022 solutions
date    : 2022-12-01
tags    : advent-of-code, purescript
summary : My solutions to the advent of code puzzles for 2022.
---

# Overview

|                 |                 |        |        |        |
| :--             | :--             | :--    | :--    | :--    |
| [day 1](#day01) | day 6           | day 11 | day 16 | day 21 |
| [day 2](#day02) | [day 7](#day07) | day 12 | day 17 | day 22 |
| day 3           | day 8           | day 13 | day 18 | day 23 |
| day 4           | day 9           | day 14 | day 19 | day 24 |
| day 5           | day 10          | day 15 | day 20 | day 25 |


 


# Day 1 - Elves & Calorie counting {#day01}

The [day 1 Advent of Code puzzle](https://adventofcode.com/2022/day/1)

Input shape:

```
1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
```

I represented the "calorie roster" specified in the puzzle using the
type `Array (Array Int)`. Populating this from the sample data, this
roster had the form:

``` haskell
roster :: Array (Array Int)
roster = [[1000,2000,3000],[4000],[5000,6000],[7000,8000,9000],[10000]]
```

The calories for a given elf were obtained by the `sum` operation.

``` haskell
elfCalories :: Array (Array Int) -> Array Int
elfCalories roster =
  sum <$> roster
  
elfCalories roster
-- -> [6000,4000,11000,24000,10000]
```

(remember that the operator `<$>` represents an infix form of the
function

```
map :: forall a b. (a -> b) -> f a -> f b
```

where `f` is a `functor` -- in our case, `f` is `Array`, the type variable
`a` is `Array Int` and the type variable `b` is `Int`.)


Then part 1 is just solved by finding the `max` value in the array resulting
from `elfCalories`. Part 2 is only a bit more complicated.

Here is the full code:

``` haskell
module Day01
  where

import Prelude
import Data.Maybe (Maybe(..))
import Data.Array (slice, sortWith, uncons, (:))
import Data.Foldable (foldr, sum)
import Data.Int (fromString)
import Data.String (split)
import Data.String.Pattern (Pattern(..))
import Effect (Effect)
import Effect.Console (log)
import Node.Encoding (Encoding(..))
import Node.FS.Sync (readTextFile)

lines :: String -> Array String
lines s = split (Pattern "\n") s

buildRoster :: Array String -> Array (Array Int)
buildRoster as =
  foldr append [[]] am
  where
    am :: Array (Maybe Int)
    am = fromString <$> as

    append :: Maybe Int -> Array (Array Int) -> Array (Array Int)
    append mi cumul =
      case uncons cumul of
        Just {head,tail} ->
          case mi of
            Just m -> (m:head):tail
            Nothing ->  []:head:tail
        Nothing -> [[]]

elves :: String -> Array (Array Int)
elves s = buildRoster $ lines s
    
elfCalories :: Array (Array Int) -> Array Int
elfCalories roster =
  sum <$> roster

maxCalories :: Array Int -> Int
maxCalories ai =
  foldr max 0 $ ai

topThreeTotal :: Array Int -> { elves:: Array Int, sum:: Int}
topThreeTotal ai =
  { sum: sum sai, elves: sai}
  where
    sai = slice 0 3 $ sortWith (\x -> (-1)*x) ai

filename :: String
filename = "assets/day01--sample"
--  filename = "assets/day01--data"

main :: Effect Unit
main = do
  sarr <- readTextFile UTF8 filename
  
  let e = elves sarr
      ec = elfCalories e
      max = maxCalories ec
      ttt = topThreeTotal ec

  log $ show e
  log $ show ec
  
  log $ "max: " <> (show max)
  log $ "TTT: " <> (show ttt)
```

On the sample data, this gives output as follows:

```

max: 24000
TTT: { elves: [24000,11000,10000], sum: 45000 }
```

On my puzzle input, the output is

```
max: 69795
TTT: { elves: [69795,69434,69208], sum: 208437 }
```


# Day 2 - Rock Paper Scissors {#day02}


Here is the [day 2 advent of code puzzle](https://adventofcode.com/2022/day/2).

Sample data:
```
A Y
B X
C Z
```

Each line represents a "turn" in Rock Paper Scissors. The left hand
column represents the Elf's move, and the right hand column represents
"my" move, under two different interpretations.

I represented moves by the data type

```
data Move = Rock
          | Paper
          | Scissors
```

Under the part 1 interpretation -- see the function `turnv1` below --
the sample input becomes:

``` haskell
[(Tuple Rock Paper),(Tuple Paper Rock),(Tuple Scissors Scissors)]
```

Under the part 2 interpretation -- see the function `turnv2` -- the
sample input becomes

``` haskell
[(Tuple Rock Rock),(Tuple Paper Rock),(Tuple Scissors Rock)]
```

The function `outcome` yields the outcome of any `Move`, and the
required scores are computed via `moveScore` and `outcomeScore`.

Here is the full code:

``` haskell
module Day02
  where

import Prelude

import Data.Array (catMaybes, (!!))
import Data.Foldable (foldr)
import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe(..))
import Data.Show.Generic (genericShow)
import Data.String (split)
import Data.String.Pattern (Pattern(..))
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Console (log)
import Node.Encoding (Encoding(..))
import Node.FS.Sync (readTextFile)

data Move = Rock
          | Paper
          | Scissors

derive instance moveGeneric :: Generic Move _

instance showMove :: Show Move where
  show = genericShow


data Outcome = ElfWin
             | IWin
             | Draw

moveScore :: Move -> Int
moveScore Rock = 1
moveScore Paper = 2
moveScore Scissors = 3

outcomeScore :: Outcome -> Int
outcomeScore ElfWin = 0
outcomeScore Draw = 3
outcomeScore IWin = 6

outcome :: Tuple Move Move -> Outcome
outcome (Tuple Rock Scissors) = ElfWin
outcome (Tuple Rock Rock) = Draw
outcome (Tuple Rock Paper) = IWin
outcome (Tuple Paper Rock) = ElfWin
outcome (Tuple Paper Paper) = Draw
outcome (Tuple Paper Scissors) = IWin
outcome (Tuple Scissors Paper) = ElfWin
outcome (Tuple Scissors Scissors) = Draw
outcome (Tuple Scissors Rock) = IWin


myScoreTurn :: Tuple Move Move -> Int -> Int
myScoreTurn (Tuple x y) cumul =
  cumul + (moveScore y) + (outcomeScore $ outcome (Tuple x y))

myScore :: Array (Tuple Move Move) -> Int
myScore = foldr myScoreTurn 0


lines :: String -> Array String
lines s = split (Pattern "\n") s

turnv1 :: String -> Maybe (Tuple Move Move)
turnv1 s = do
  ec <- res !! 0
  mc <- res !! 1
  em <- getElfMove ec
  mm <- getMyMove mc
  pure $ Tuple em mm
  where
    res :: Array String
    res = split (Pattern " ") s

    getElfMove :: String -> Maybe Move
    getElfMove t = case t of
      "A" -> Just Rock
      "B" -> Just Paper
      "C" -> Just Scissors
      _ -> Nothing

    getMyMove :: String -> Maybe Move
    getMyMove t = case t of
      "X" -> Just Rock
      "Y" -> Just Paper
      "Z" -> Just Scissors
      _ -> Nothing

turnsv1 :: Array String -> Array (Tuple Move Move)
turnsv1 as =
  catMaybes res
  where
    res = turnv1 <$> as


requirement :: Move -> Outcome -> Move
requirement Rock ElfWin = Scissors
requirement Rock Draw = Rock
requirement Rock IWin = Paper
requirement Scissors ElfWin = Paper
requirement Scissors Draw = Scissors
requirement Scissors IWin = Rock
requirement Paper ElfWin = Rock
requirement Paper Draw = Paper
requirement Paper IWin = Scissors

turnv2 :: String -> Maybe (Tuple Move Move)
turnv2 s = do
  ec <- res !! 0
  mc <- res !! 1
  em <- getElfMove ec
  mm <- getOutcome mc
  pure $ Tuple em (requirement em mm)
  where
    res :: Array String
    res = split (Pattern " ") s

    getElfMove :: String -> Maybe Move
    getElfMove t = case t of
      "A" -> Just Rock
      "B" -> Just Paper
      "C" -> Just Scissors
      _ -> Nothing

    getOutcome :: String -> Maybe Outcome
    getOutcome t = case t of
      "X" -> Just ElfWin
      "Y" -> Just Draw
      "Z" -> Just IWin
      _ -> Nothing
  

turnsv2 :: Array String -> Array (Tuple Move Move)
turnsv2 as =
  catMaybes res
  where
    res = turnv2 <$> as

filename :: String
filename = "assets/day02--data"
-- filename = "assets/day02--sample"

main :: Effect Unit
main = do
  sarr :: Array String <- lines <$> readTextFile UTF8 filename

  let t1 = turnsv1 sarr
  -- log $ "Turns part 1: " <> (show t1)      
  log $ "Score part 1: " <> (show $ myScore t1)

  let t2 = turnsv2 sarr
  -- log $ "Turns part 2: " <> (show t2)      
  log $ "Score part 2: " <> (show $ myScore t2)
```

The output on the sample data is

```
Score part 1: 15
Score part 2: 12
```

And the output on the puzzle input is

```
Score part 1: 10595
Score part 2: 9541
```

# Day 7 - "No Space Left on Device" {#day07}

I wanted to model the file-system for the "device" of this problem
using a tree:

``` haskell
data Dir = Dir { name :: String
               , files :: Map String File
               , subdirs :: Map String Dir
               }
```

But I realized that with this structure, given a node one can't find
its *parent* without some further information. I burned some time
thinking about how to deal with this before googling a bit and
realizing that there is a functional-programming pattern for dealing
with this sort of matter --
[*zippers*](https://wiki.haskell.org/Zipper) -- which I was somehow
unaware of!

To be honest, I skimmed [this day07 solution
here](https://notes.abhinavsarkar.net/2022/aoc-7), which showed me a
good model for the zipper-usage I needed. While I "cheated" a bit, I
learned something that feels really useful (and much more useful to me
than cobbling together a more imperative style solution to the
puzzle). I was happy to see that outside of the zipper usage, I had
produced essentially the same code!

``` haskell
module Day07
  where

import Parsing (Parser, runParser)
import Prelude

import Control.Alt ((<|>))
import Data.Array (concat, filter, foldRecM, fromFoldable, sort, uncons, (:))
import Data.CodePoint.Unicode (isSpace)
import Data.Either (hush)
import Data.Foldable (intercalate, sum)
import Data.Generic.Rep (class Generic)
import Data.Map (Map, values)
import Data.Map as Map
import Data.Maybe (Maybe(..))
import Data.Show.Generic (genericShow)
import Data.String (CodePoint)
import Effect (Effect)
import Effect.Console (log)
import Node.Encoding (Encoding(..))
import Node.FS.Sync (readTextFile)
import Parsing.Combinators.Array (many, many1)
import Parsing.String (string)
import Parsing.String.Basic (intDecimal, space, takeWhile)

type File = { filename :: String
            , size :: Int
            }


data FD = F File
        | D String

derive instance fdGeneric :: Generic FD _

instance showFD :: Show FD where
  show = genericShow


data Action = Cd String
            | Cdup
            | Ls (Array FD)

derive instance actionGeneric :: Generic Action _

instance showAction :: Show Action where
  show = genericShow

    
isNotWS :: CodePoint -> Boolean
isNotWS cp = not $ isSpace cp

cd :: Parser String Action
cd = do
  _ <- string "$ cd"
  _ <- many1 space
  name <- takeWhile isNotWS
  _ <- many space
  pure $ Cd $ name

cdup :: Parser String Action
cdup = do
  _ <- string "$ cd .."
  _ <- many space
  pure Cdup

file :: Parser String FD
file = do
  sz <- intDecimal
  _ <- many1 space
  filename <- takeWhile isNotWS
  _ <- many space
  pure $ F {size:sz, filename}

dir :: Parser String FD
dir = do
  _ <- string "dir "
  name <- takeWhile isNotWS
  _ <- many space  
  pure $ D name

ls :: Parser String Action
ls = do
  _ <- string "$ ls"
  _ <- many1 space
  content <- fromFoldable <$> many (file <|> dir) 
  pure $ Ls $ fromFoldable content

actions :: Parser String (Array Action)
actions = many act
  where
    act :: Parser String Action
    act = ls <|> cdup <|> cd

--------------------------------------------------------------------------------

data Dir = Dir { name :: String
               , files :: Map String File
               , subdirs :: Map String Dir
               }

getFiles :: Map String File -> Array File
getFiles m = fromFoldable $ Map.values m

getSubdirs :: Map String Dir -> Array Dir
getSubdirs m = fromFoldable $ Map.values m

displayDir :: Dir -> String
displayDir (Dir { name, files, subdirs } ) =
  intercalate "\n"
  $  [ name ] <> f <> d
  where f :: Array String
        f = show <$> getFiles files

        d :: Array String
        d = displayDir <$> getSubdirs subdirs

type Zipper = { path :: Array Dir
              , current :: Dir
              }

emptyDir :: String ->  Dir
emptyDir name = Dir { name
                    , files: Map.fromFoldable []
                    , subdirs: Map.fromFoldable []
                    }

base :: Dir
base = emptyDir "/"

moveUp :: Zipper -> Maybe Zipper
moveUp { path, current: Dir curr } =
  case uncons path of
    Nothing -> Just { path, current: Dir curr }
    Just {head: Dir h, tail} -> Just { path: tail
                                     , current: Dir $ h { subdirs = Map.insert curr.name (Dir curr) h.subdirs }
                                     }
                         
moveDown :: String -> Zipper -> Maybe Zipper
moveDown name { path, current: Dir curr } = do
  new <- Map.lookup name curr.subdirs  
  pure { path: (Dir curr):path, current: new }


moveToRoot :: Zipper -> Maybe Zipper
moveToRoot { path, current } =
  case path of
    [] -> Just { path, current}
    _ -> moveUp { path, current } >>= moveToRoot

addFile :: File -> Zipper -> Zipper
addFile f { path, current: Dir curr } =
  { path
  , current: Dir curr { files = Map.insert f.filename f curr.files }
  }

addDir :: Dir -> Zipper -> Zipper
addDir (Dir d) { path, current: Dir curr } =
  { path
  , current: Dir curr { subdirs = Map.insert d.name (Dir d) curr.subdirs }
  }

toZipper :: Dir -> Zipper
toZipper d = { path: [], current: d }

fromZipper :: Zipper -> Maybe Dir
fromZipper zipper = case (moveToRoot zipper) of
  Just { current } -> Just current
  Nothing -> Nothing


perform :: Zipper -> Action -> Maybe Zipper
perform zipper action =
  case action of
    Cd "/" -> moveToRoot zipper
    Cd name -> moveDown name zipper
    Cdup -> moveUp zipper
    Ls content ->
      foldRecM update zipper content
      where
        update :: Zipper -> FD -> Maybe Zipper
        update { path, current: Dir curr } (F f) =
          Just { path
               , current: Dir curr { files = Map.insert f.filename f curr.files }}
        update { path, current: Dir curr } (D sdname) =
          Just { path
               , current: Dir curr { subdirs = Map.insert sdname (emptyDir sdname) curr.subdirs }}
                   
allSubdirs :: Dir -> Array Dir
allSubdirs (Dir d) =
  (Dir d):strict
  where
    strict :: Array Dir
    strict =  concat $ allSubdirs <$> (fromFoldable $ values d.subdirs)
    
size :: Dir -> Int
size (Dir d) =
  b + rest
  where
    b = sum $ _.size <$> d.files
    rest = sum $ size <$> d.subdirs


getResult :: String -> Maybe Dir
getResult aa = do
  a <- hush $ runParser aa actions
  let bb = toZipper base
  rr <- foldRecM perform bb a
  r <- fromZipper rr
  pure r

part1 :: Dir -> Int
part1 dd =
  sum ssizes
  where
    sizes = size <$> allSubdirs dd

    ssizes = filter f sizes
    
    f :: Int -> Boolean
    f d = d <= 100000
  
part2 :: Dir -> Int
part2 dd =
  case uncons sizes of
    Nothing -> -1
    Just {head} -> head
  where
    unused = 70000000 - size dd

    needed = 30000000 - unused

    f :: Int -> Boolean
    f a = a >= needed

    sizes = sort $ filter f $ size <$> allSubdirs dd


datafile :: String
datafile = "assets/day07--data"
           
main :: Effect Unit
main = do
  all  <- readTextFile UTF8 datafile

  case (getResult all) of
    Nothing -> log "error"
    Just d -> do

      log $ "part 1: " <> (show $ part1 d)
      log $ "part 2: " <> (show $ part2 d)
```
