module Main where

import Prelude

data Formula = Var String
             | And Formula Formula
             | Or Formula Formula
             | Not Formula
             | Implies Formula Formula

data Value = Value String Bool

lookup' :: [ Value ] -> String -> Maybe Bool
lookup' [] _ = Nothing
lookup' ((Value s b) : tail) t = if s==t then Just b else lookup' tail t

eval :: [ Value ] -> Formula -> Maybe Bool
eval vals (Var s) = lookup' vals s
eval vals (Not p) = do
  result <- eval vals p
  pure $ not result
eval vals (And p q) = do
  pp <- eval vals p
  qq <- eval vals q
  pure $ pp && qq
eval vals (Or p q)  = do
  pp <- eval vals p
  qq <- eval vals q
  pure $ pp || qq
eval vals (Implies p q) = do
  pp <- eval vals p
  qq <- eval vals q
  pure $ not pp || qq

--  here is a formula

f :: Formula
f = And (Var "p") (Or (Var "q") (And (Not (Var "r")) (Var "p")))

-- and here are values for the propositional variables

values =  [ Value "p" True
          , Value "q" False
          , Value "r" True
          ]

-- and this will evaluate the formula

result :: Maybe Bool
result = eval values f

-- should evaluate to: Just False

-- λ> result
-- Just False
-- λ> 

main :: IO ()
main =
  putStrLn ""
